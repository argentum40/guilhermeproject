
// constants won't change. Used here to set a pin number:
const int vib1Pin =  5;// the number of the LED pin
const int vib2Pin =  6;// the number of the LED pin

// Variables will change:
int vib1State = LOW;             // ledState used to set the LED
int vib2State = LOW;             // ledState used to set the LED

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis1 = 0;        // will store last time LED was updated
unsigned long previousMillis2 = 0;        // will store last time LED was updated

// constants won't change:
const long interval1 = 1500;           // interval at which to blink (milliseconds)
const long interval2 = 3000;           // interval at which to blink (milliseconds)

void setup() {
  // set the digital pin as output:
  pinMode(vib1Pin, OUTPUT);
  pinMode(vib2Pin, OUTPUT);
}

void loop() {
  // here is where you'd put code that needs to be running all the time.

  // check to see if it's time to blink the LED; that is, if the difference
  // between the current time and last time you blinked the LED is bigger than
  // the interval at which you want to blink the LED.
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis1 >= interval1) {
    // save the last time you blinked the LED
    previousMillis1 = currentMillis;
    // if the LED is off turn it on and vice-versa:
    if (vib1State == LOW) {
      vib1State = HIGH;
    } else {
      vib1State = LOW;
    }
  }

if (currentMillis - previousMillis2 >= interval2) {
    // save the last time you blinked the LED
    previousMillis2 = currentMillis;
    // if the LED is off turn it on and vice-versa:
    if (vib2State == LOW) {
      vib2State = HIGH;
    } else {
      vib2State = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(vib1Pin, vib1State);
    digitalWrite(vib2Pin, vib2State);
  }
}
